from pom_pages.base_page import BasePage
from locator import Locator

class InventoryPage(BasePage):
	
	def __init__(self, driver):
		super().__init__(driver)
		self.url = 'https://www.saucedemo.com/inventory.html'

	def go_back_to_login(self):
		self.driver.back()
		self.wait_until_abscent(Locator.app_logo)

