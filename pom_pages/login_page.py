from pom_pages.base_page import BasePage
from locator import Locator
from selenium.webdriver.common.keys import Keys

class LoginPage(BasePage):

	def __init__(self, driver):
		super().__init__(driver)

	def login(self, username, password):
		self.find(Locator.username_input).send_keys(username)
		self.find(Locator.password_input).send_keys(password)
		self.find(Locator.login_button).click()

	def get_error_message_text(self):
		return self.find(Locator.error_message).text	

	def close_error_banner(self):
		self.find(Locator.cross_button).click()
		self.wait_until_abscent(Locator.error_message)


