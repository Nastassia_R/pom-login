from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

class BasePage(object):

    def __init__(self, driver):
        self.driver = driver
        self.wait = WebDriverWait(self.driver, 10)

    def wait_for(self, locator):
        return self.wait.until(EC.presence_of_element_located(locator))

    def find(self, locator):
        return self.driver.find_element(*locator)

    def find_list_of_elements(self, locator):
        return self.driver.find_elements(*locator)

    def wait_until_abscent(self, locator):
        self.wait.until(EC.invisibility_of_element_located(locator))

    def get_url(self):
        return self.driver.current_url

    def refresh_page(self):
        self.driver.refresh()

    def get_element_text(self, locator):
        element = self.find(locator)
        return element.text



