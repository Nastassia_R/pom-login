from selenium.webdriver.common.by import By

class Locator(object):
	username_input = (By.XPATH, "//*[@id='user-name']")
	password_input = (By.XPATH, "//*[@id='password']")
	login_button = (By.XPATH, "//*[@id='login-button']")
	error_message = (By.XPATH, "//h3[@data-test='error']")
	cross_button = (By.XPATH, "//button[@class='error-button']")
	app_logo = (By.XPATH, "//*[@class='app_logo']")
	inventory_item = (By.XPATH, "//div[@class='inventory_item']")
	item_name = (By.XPATH, "//*[@class='inventory_item_name ']")
	item_description = (By.XPATH, "//*[@class='inventory_item_desc']")
	item_price = (By.XPATH, "//*[@class='inventory_item_price']")
	add_to_cart_button = (By.XPATH, "//*[contains(@data-test, 'add-to-cart')]")
	cart_link = (By.XPATH, "//*[@class='shopping_cart_link']")

	