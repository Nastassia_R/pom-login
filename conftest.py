import pytest
from selenium import webdriver

url = 'https://www.saucedemo.com/'
browser = 'Chrome'

@pytest.fixture(scope="session")
def driver():
    if browser == 'Chrome':
        driver = webdriver.Chrome()
    elif browser == 'Firefox':
        driver = webdriver.Firefox()
    else:
        raise ValueError(f"Unsupported browser: {browser}")
    driver.get(url)
    yield driver
    driver.quit()

    