import pytest
from pom_pages.login_page import LoginPage
from pom_pages.inventory_page import InventoryPage
from messages import Messages
from credentials import Credentials

class TestLoginPageSuite:

    @pytest.fixture(scope="class")
    def log(self, driver):
        """
        A fixture that instantiates the LoginPage object and allows the use of its methods.
        """
        return LoginPage(driver)

    @pytest.fixture(scope="class")
    def inventory(self, driver):
        """
        A fixture that instantiates the InventoryPage object and allows the use of its methods.
        """
        return InventoryPage(driver)

    def test_valid_login(self, log, inventory):
        """
        This test case performs a login using the login method of the LoginPage class with
        standard_user and user_password attributes of the Credentials class. It verifies that
        the current page URL is equal to the expected URL attribute for the InventoryPage class.
        It then navigates back to the login page using the go_back_to_login method of the
        InventoryPage class.
        """
        log.login(Credentials.standard_user, Credentials.user_password)
        assert inventory.url == inventory.get_url()
        inventory.go_back_to_login()

    @pytest.mark.parametrize("username, password, message", [
        (Credentials.locked_out_user, Credentials.user_password, Messages.locked_user_message),
        (Credentials.standard_user, Credentials.invalid_user_password, Messages.invalid_password_message),
        (Credentials.standard_user, '', Messages.missing_password_message),
        ('', Credentials.user_password, Messages.missing_username_message)
    ])
    def test_with_invalid_data(self, log, username, password, message):
        """
        This parametrized test case performs invalid logins for various scenarios: locked-out username,
        invalid password, empty username, and empty password. The refresh_page method of BasePage
        is used to clear inputs before each execution. The login method of LoginPage class is used
        to enter data into input fields. The error message text is retrieved using the get_error_message_text
        method of LoginPage class and compared to the corresponding attribute of the Messages class.
        The close_error_banner method of LoginPage class is used to close the banner before the next iteration.
        """
        log.refresh_page()
        log.login(username, password)
        assert log.get_error_message_text() == message
        log.close_error_banner()


