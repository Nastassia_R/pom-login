class Messages(object):
	locked_user_message = 'Epic sadface: Sorry, this user has been locked out.'
	invalid_password_message = 'Epic sadface: Username and password do not match any user in this service'
	missing_password_message = 'Epic sadface: Password is required'
	missing_username_message = 'Epic sadface: Username is required'
