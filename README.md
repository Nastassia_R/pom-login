**Summary**
Multi-platform sample web test suite designed according with page-object model using PyTest and Selenium

**Pytest and Selenium Test Suite**
This repository contains a Pytest and Selenium test suite for testing the login form of https://the-internet.herokuapp.com

The testing process in this repository is implemented using the Page Object Model (POM) design pattern.

The following items were added for this suite:

- pom page folder that contains page object classes including:
1. BasePage class which is parent class for all page classes and contains all methods that are common between page objects
2. LoginPage class which contains methods to test the following scenarios: login with valid username and password, login with locked_out_user, login with invalid password, login with empty username/password
3. InventoryPage class which contains methods and attributes for inventory page
- Locator class that contains locator strategies and locators for all elements on Login and Inventory page
- Credential class that contains input data for all login scenarios
- Message class that contains error messages
- conftest.py file that contains driver fixture that creates webdriver objects in the session start and quits the browser when the session is over
- test_login_form.py file that contains TestLoginPageSuite class which has all scenarios above listed as a test_ methods
- pytest.ini file that specifies configuration for pytest including running options and naming conventions
- requirements.txt file which contains all setup pre-requisites
- HTML report is generated in output folder


**Author**
Nastassia Rabkina
